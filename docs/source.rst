source package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   source.utilities

Submodules
----------

source.random\_walk module
--------------------------

.. automodule:: source.random_walk
   :members:
   :undoc-members:
   :show-inheritance:

source.simulator module
-----------------------

.. automodule:: source.simulator
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: source
   :members:
   :undoc-members:
   :show-inheritance:

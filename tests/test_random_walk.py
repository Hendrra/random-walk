import functools
import unittest

import ddt

from source.random_walk import RandomWalk
from source.random_walk import SingleRandomWalk
from source.utilities.calculators import StandardCoordinatesCalculator
from source.utilities.randomizers import dummy_randomizer


@ddt.ddt
class TestSingleRandomWalk(unittest.TestCase):

    def setUp(self):
        self.random_walk = functools.partial(
            SingleRandomWalk, randomizer=dummy_randomizer, calculator=StandardCoordinatesCalculator
        )

    @ddt.unpack
    @ddt.data(
        (0, 1),
        (5, 6),
    )
    def test_returns_proper_length(self, rounds, length):
        x, y = self.random_walk(x_start=0, y_start=0, rounds=rounds, d=1).coordinates()
        self.assertEqual(len(x), length)
        self.assertEqual(len(x), length)


@ddt.ddt
class TestRandomWalk(unittest.TestCase):

    def setUp(self):
        self.random_walk = functools.partial(
            RandomWalk, randomizer=dummy_randomizer, calculator=StandardCoordinatesCalculator
        )
        self.paths = self.random_walk(x_start=[0, 0], y_start=[0, 0], rounds=10, d=[1, 1]).paths()

    def test_returns_proper_type(self):
        # checks whether the main type is proper
        self.assertEqual(type(self.paths), list)
        for path in self.paths:
            # checks whether the inner types are computed correctly
            self.assertEqual(type(path), tuple)

    def test_returns_proper_amount_of_data(self):
        # checks whether there are paths for 2 blobs
        self.assertEqual(len(self.paths), 2)
        for path in self.paths:
            # checks whether each blob contains x coordinates and y coordinates inside its path
            self.assertEqual(len(path), 2)
            x, y = path
            # checks whether the number of x coordinates is correct
            self.assertEqual(len(x), 11)
            # checks whether the number of y coordinates is correct
            self.assertEqual(len(y), 11)

    def test_raises_error_when_lengths_differ(self):
        with self.assertRaises(ValueError) as ctx:
            self.random_walk(x_start=[0], y_start=[0, 0], rounds=10, d=[1])
        self.assertEqual(str(ctx.exception), 'The path should contain the same amount of x and y coordinates!')

    @ddt.unpack
    @ddt.data(
        [[0, 1, 10]],
        [[1, 1, -5]],
    )
    def test_raises_error_when_distance_is_non_positive(self, d):
        with self.assertRaises(ValueError) as ctx:
            self.random_walk(x_start=[0], y_start=[0], rounds=2, d=d)
        self.assertEqual(str(ctx.exception), 'The distance between two points must be non-negative!')

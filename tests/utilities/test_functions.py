import math
import unittest

import ddt

from source.utilities.functions import iterated_logarithm


@ddt.ddt
class TestIteratedLogarithm(unittest.TestCase):

    @ddt.unpack
    @ddt.data(
        (math.e, 0),
    )
    def test_computes_proper_value(self, n, expected):
        computed = iterated_logarithm(n=n)
        self.assertAlmostEqual(computed, expected)

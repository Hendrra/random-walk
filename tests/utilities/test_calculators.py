import math
import unittest

import ddt

from source.utilities.calculators import StandardCoordinatesCalculator


@ddt.ddt
class TestCoordinatesCalculator(unittest.TestCase):

    def setUp(self):
        self.calculator = StandardCoordinatesCalculator

    @ddt.unpack
    @ddt.data(
        (0, 0, True),
        (0, 0, False),
        (30, 30, False),
        (30, math.pi/6, True),
    )
    def test_handles_angle_units(self, angle, expected_psi, degrees):
        computed_psi = self.calculator(x=0, y=0, d=1, angle=angle, degrees=degrees).psi
        self.assertAlmostEqual(computed_psi, expected_psi)

    @ddt.unpack
    @ddt.data(
        (0, 0, 1, 0),
        (1, 1, 1, 0),
        (0, 0, 1, 45),
        (1, 1, 1, 45),
    )
    def test_calculates_values_at_appropriate_distance(self, x0, y0, d, angle):
        x, y = self.calculator(x=x0, y=y0, d=d, angle=angle).calculate()
        r = (x0 - x)**2 + (y0 - y)**2
        self.assertAlmostEqual(r, d**2)

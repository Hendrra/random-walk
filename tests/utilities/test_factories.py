import functools
import unittest

import ddt

from source.utilities.factories import CalculatorFactory
from source.utilities.factories import ParameterFactory
from source.utilities.factories import PlotterFactory
from source.utilities.factories import RandomizerFactory


@ddt.ddt
class TestCalculatorFactory(unittest.TestCase):

    def setUp(self):
        self.factory = CalculatorFactory

    @ddt.unpack
    @ddt.data(
        ['standard', 'StandardCoordinatesCalculator'],
    )
    def test_returns_proper_object(self, method, expected_object):
        computed_object = self.factory(method=method).produce().__name__
        self.assertEqual(computed_object, expected_object)

    @ddt.data(
        'standard',
    )
    def test_not_raises_error_when_proper_rand_method_is_typed(self, method):
        try:
            self.factory(method=method).produce()
        except ValueError:
            self.fail('ValueError should not be raised!')

    def test_raises_error_when_wrong_calc_method_is_typed(self):
        with self.assertRaises(ValueError) as ctx:
            self.factory(method='Method').produce()
        self.assertEqual(str(ctx.exception), 'Supported methods: standard!')


@ddt.ddt
class TestRandomizerFactory(unittest.TestCase):

    def setUp(self):
        self.factory = RandomizerFactory

    @ddt.unpack
    @ddt.data(
        ['brownian', 'randomizer_for_brownian_motion'],
        ['stochastic', 'randomizer_for_stochastic_process'],
    )
    def test_returns_proper_object(self, method, expected_object):
        computed_object = self.factory(method=method).produce().__name__
        self.assertEqual(computed_object, expected_object)

    @ddt.unpack
    @ddt.data(
        ['brownian'],
        ['stochastic'],
    )
    def test_not_raises_error_when_proper_rand_method_is_typed(self, method):
        try:
            self.factory(method=method).produce()
        except ValueError:
            self.fail('ValueError should not be raised!')

    def test_raises_error_when_wrong_rand_method_is_typed(self):
        with self.assertRaises(ValueError) as ctx:
            self.factory(method='Method').produce()
        self.assertEqual(str(ctx.exception), 'Supported methods: brownian and stochastic!')


@ddt.ddt
class TestParameterFactory(unittest.TestCase):

    def setUp(self):
        self.factory = functools.partial(
            ParameterFactory,
            step=1, x_bounds=[-1, 1], y_bounds=[-1, 1], initial_x=[0, 1], initial_y=[0, 1], steps=[1, 2]
        )

    @ddt.data(
        'standard',
        'random',
        'custom',
    )
    def test_returns_proper_type(self, method):
        self.assertEqual(type(self.factory(method=method, blobs=1).produce()), dict)

    @ddt.data(
        'standard',
        'random',
        'custom',
    )
    def test_returns_proper_keys(self, method):
        self.assertCountEqual(self.factory(method=method, blobs=1).produce().keys(), ['x_start', 'y_start', 'd'])

    def test_returns_proper_values_for_standard_method(self):
        parameters = self.factory(method='standard', blobs=1).produce()
        self.assertEqual(parameters['x_start'], [0])
        self.assertEqual(parameters['y_start'], [0])
        self.assertEqual(parameters['d'], [1])

    def test_returns_proper_values_for_random_method(self):
        parameters = self.factory(method='random', blobs=1).produce()
        self.assertGreaterEqual(parameters['x_start'][0], -1)
        self.assertLess(parameters['x_start'][0], 1)
        self.assertGreaterEqual(parameters['y_start'][0], -1)
        self.assertLess(parameters['y_start'][0], 1)
        self.assertEqual(parameters['d'], [1])

    @ddt.unpack
    @ddt.data(
        # if the amount of blobs is less than the number of starting points/steps they will be trimmed,
        # the trimming is done just by slicing the list
        {
            'blobs': 1,
            'x_start': [0],
            'y_start': [0],
            'd': [1],
        },
        # the amount of blobs is equal to the number of starting points/steps, thus each blob will
        # be mapped to the corresponding value
        {
            'blobs': 2,
            'x_start': [0, 1],
            'y_start': [0, 1],
            'd': [1, 2],
        },
        # if the amount of blobs is greater than the number of starting points/steps the list
        # will be expanded with the last value
        {
            'blobs': 3,
            'x_start': [0, 1, 1],
            'y_start': [0, 1, 1],
            'd': [1, 2, 2],
        },
    )
    def test_returns_proper_values_for_custom_method(self, blobs, x_start, y_start, d):
        parameters = self.factory(method='custom', blobs=blobs).produce()
        self.assertEqual(parameters['x_start'], x_start)
        self.assertEqual(parameters['y_start'], y_start)
        self.assertEqual(parameters['d'], d)


@ddt.ddt
class TestPlotterFactory(unittest.TestCase):

    def setUp(self):
        self.factory = PlotterFactory

    @ddt.unpack
    @ddt.data(
        ['static', 'StaticPlotter'],
        ['animated', 'AnimatedPlotter'],
    )
    def test_returns_proper_object(self, method, expected_object):
        computed_object = self.factory(method=method).produce().__name__
        self.assertEqual(computed_object, expected_object)

    @ddt.data(
        'static',
        'animated',
    )
    def test_not_raises_error_when_proper_rand_method_is_typed(self, method):
        try:
            self.factory(method=method).produce()
        except ValueError:
            self.fail('ValueError should not be raised!')

    def test_raises_error_when_wrong_calc_method_is_typed(self):
        with self.assertRaises(ValueError) as ctx:
            self.factory(method='Method').produce()
        self.assertEqual(str(ctx.exception), 'Supported methods: static and animated!')

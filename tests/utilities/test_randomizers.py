import unittest

import ddt

from source.utilities.randomizers import random_float


@ddt.ddt
class TestRandomFloat(unittest.TestCase):

    def setUp(self):
        self.randomizer = random_float

    def test_returns_proper_type(self):
        number = self.randomizer(left_bound=0, right_bound=10)
        self.assertEqual(type(number), float)

    @ddt.unpack
    @ddt.data(
        (0, 1),
        (0, 10),
        (0, 100),
        (-1, 0),
        (-10, 0),
        (-100, 0),
    )
    def test_returns_number_in_proper_range(self, left_bound, right_bound):
        number = self.randomizer(left_bound=left_bound, right_bound=right_bound)
        self.assertGreaterEqual(number, left_bound)
        self.assertLess(number, right_bound)

    def test_raises_error_when_left_bound_is_greater_than_right_bound(self):
        with self.assertRaises(ValueError) as ctx:
            self.randomizer(left_bound=1, right_bound=0)
        self.assertEqual(str(ctx.exception), 'Left bound cannot be greater than right bound!')

import unittest

from source.parser import Parser


class TestParser(unittest.TestCase):

    def setUp(self):
        self.arguments = Parser.parse(arguments='-b 1 -r 10 -s standard'.split())

    def test_sets_proper_default_value_step(self):
        self.assertEqual(self.arguments.step, 1)

    def test_sets_proper_default_value_rand_method(self):
        self.assertEqual(self.arguments.rand_method, 'brownian')

    def test_sets_proper_default_value_calc_method(self):
        self.assertEqual(self.arguments.calc_method, 'standard')

    def test_sets_proper_default_value_x_bounds(self):
        self.assertEqual(self.arguments.x_bounds, (-20, 20))

    def test_sets_proper_default_value_y_bounds(self):
        self.assertEqual(self.arguments.y_bounds, (-20, 20))

    def test_sets_proper_default_value_initial_x(self):
        self.assertEqual(self.arguments.initial_x, None)

    def test_sets_proper_default_value_initial_y(self):
        self.assertEqual(self.arguments.initial_y, None)

    def test_sets_proper_default_value_steps(self):
        self.assertEqual(self.arguments.initial_y, None)

    def test_sets_proper_default_value_plot_method(self):
        self.assertEqual(self.arguments.plot_method, 'static')

    def test_sets_proper_default_value_link(self):
        self.assertEqual(self.arguments.link, True)

    def test_sets_proper_default_value_annotate(self):
        self.assertEqual(self.arguments.annotate, False)

    def test_sets_proper_default_value_grid(self):
        self.assertEqual(self.arguments.grid, True)

    def test_sets_proper_default_value_save(self):
        self.assertEqual(self.arguments.save, False)

    def test_sets_proper_default_value_interval(self):
        self.assertEqual(self.arguments.interval, 100)

    def test_sets_proper_default_value_iterated_logarithm(self):
        self.assertEqual(self.arguments.save, False)

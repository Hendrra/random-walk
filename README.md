# Random Walk

## Purpose
As a mathematician I am really into randomness, stochastic processes and other probabilistic stuff. To be honest I am
in love with the concepts I have just described since I was in the secondary school.

However, I work as a Python programmer thus I thought that it can be a nice exercise to code my own simulator of 
a random walk. I have even gone a step further and created a mechanism that is able to simulate random wandering in its 
two, most familiar forms, i.e. Brownian motion and a stochastic process.

## Installation
There are no special needs or requirements to install this tool.

To use the code provided here you just need to:
1. clone this repository using `git clone`,
2. install all the requirements using `pip install -r requirements.txt` from the parent directory.

Please note that I used Python 3.10.6 during the development however all versions 3+ will be suitable.

## Usage
After introducing the `1.1` version it is possible to run the simulation directly from the terminal and there's no need
to change, add or modify anything in the source code unless you need some serious customisation. 

Basically you can run the simulation with the following command from the parent directory.
```
python simulate.py -b 1 -r 10 -s standard -st 1
```
That command will create the simulation containing just one blob and the length of the simulation will be equal to 11
(10 steps + initial one) steps in total. The starting conditions are equal to standard which means that the point will 
start its journey at the origin of the coordinate system and each step will be exactly one unit away. Please note that 
this is a very simple and basic case. Please read the description of other parameters, so you can perform some amazing
simulations.

The complete list of parameters which can be used to generate a random walk simulation:

1. `--blobs` or `-b`: number of points participating in a random walk, this is a required parameter, and it should an 
integer, this parameter is **required**,
2. `--rounds` or `-r`: the length of the path for each blob (the length of the walk), this is equivalent to the fact
that the trajectory of each point will contain `rounds+1` spots (the number of rounds + the initial position), this is 
a required parameter, and it should an integer, this parameter is **required**,
3. `--start` or `-s` the method of computing the starting conditions for points involved in the random walk, 
this parameter is **required**, there are several options provided:
    1) `standard`: the most expected method for creating random walks, here we have specified amount of points
    (argument `blobs`), each point has the same step length (argument `d`), moreover all the points start from
    the same starting coordinates, i.e. `(0, 0)`. Please note that this method requires the following arguments:
    `blobs` and `d`,
    2) `random`: very similar to standard with one major difference -- starting points are randomly chosen for
    each of the point. Please note that this method requires the following arguments: `blobs` and `d`. You can also
    specify `x_bounds` and `y_bounds` (by default a square, for exact dimensions please use `--help`),
    3) `custom`: you can customise everything, i.e. you can manually set the starting coordinates for both axis,
    you can also manually set the length of the step for each point, if you select this method you need to
    provide the following arguments: `blobs`, `initial_x`, `initial_y` and `steps`. Note! If any of those lists contains
    fewer elements than the value of blobs than the list will be completed with the last element from it which
    was provided by the user. For example let's assume that the user typed the following arguments: `blobs=5`
    and `initial_x=[0, 1, 2]`. In such cases random walk will be computed with `initial_x=[0, 1, 2, 2, 2]`,  
4. `--step` or `-st` the distance between two steps, this parameter is used for `start=standard` and `start=random`,
5. `--rand-method` or `-rm`: the type of the random walk, for now there are two possibilities:
    1) brownian: standard random walk, i.e. brownian motion(s) will be plotted,
    2) stochastic: the class will plot standard stochastic process(es),
6. `--calc-method` or `-cm`: the method of computing new, next points in the random walk; for now there
is only one possibility:
    1) standard: for further details please check the description inside StandardCoordinatesCalculator class,
7. `--x-bounds` or `-xb`: the boundaries for x coordinates, i.e. when `start=random` the x coordinates are drawn
between the specified numbers,
8. `--y-bounds` or `-yb`: the boundaries for y coordinates, i.e. when `start=random` the y coordinates are drawn
between the specified numbers,
9. `--initial_x` or `-ix`: the list of initial x coordinates, you need to specify the list if and only if `start=custom`,
10. `--initial_y` or `-iy`: the list of initial y coordinates, you need to specify the list if and only if `start=custom`,
11. `--steps` or `-sts`: the list of the lengths of the steps for each point, you need to specify this argument
if and only if `start=custom`,
12. `--plot_method` or `-pm`: variable which indicates the type of the plot, for now there's one possibility:
    1) static: just the final result of the simulation is going to be plotted,
13. `--link` or `--no-link`: indicates whether the points are to be connected. In other words if set to True the points 
participating in a random walk will be linked. They will form a path,
14. `--annotate` or `--no-annotate`: indicates whether the points are to be labelled. In other words if set to True 
the points participating in the random walk will be denoted by consecutive natural numbers,
15. `--grid` or `--no-grid`: indicates whether the graph should have a grid,
16. `--save` or `--no-save`: indicates whether the figure should be saved, if True the figure will be saved in the 
figures/ directory,
17. `--interval` or `-i`: Delay between frames in milliseconds. Used if and only if the plot method is equal to 
`animated`,
18. `--iterated-logarithm` or  `--no-iterated-logarithm`: adds the iterated logarithm curve.  Please note that this 
makes sense if and only if the round method is equal to stochastic.

## Testing
Sometimes it is a good practise to test whether everything has been cloned correctly or if your changes haven't caused
any serious errors. In such cases you may want to run the following command from the parent directory.
```
pytest tests/
```
The tests are able to check whether everything works as expected and whether your random walk will be generated
correctly.

## Examples
This example depicts a brownian motion (random walk) performed by a single point (blob). This simulation used the
standard starting method thus the starting point was equal to the origin of the coordinate system. Moreover, the 
simulation consists of 11 steps (starting point + 11 rounds). It was created by the command below.
```
python simulate.py -b 1 -r 10 -s standard -st 1 -rm brownian --save
```
![Image](examples/1_point_standard_brownian_no_annotation.png "Example 1")

The example below was generated by the same command as the one above however the path was annotated. It was done by
adding a special flag, please see the command below.
```
python simulate.py -b 1 -r 10 -s standard -st 1 -rm brownian --save --annotate --no-grid
```
![Image](examples/1_point_standard_brownian_annotation.png "Example 2")

The next example shows Brownian motion for many blobs with much more rounds. In such cases adding annotations might make 
the figure unreadable.
```
python simulate.py -b 10 -r 100 -s standard -st 1 -rm brownian --save 
```
![Image](examples/100_points_standard_brownian.png "Example 3")

This example depicts Brownian motion for 100 particles however they start at random positions. Their starting points are
taken from a rectangle with the following vertices: `(0, 0)`, `(100, 0)`, `(50, 0)` and `(100, 50)`.
```
python simulate.py -b 100 -r 10 -s random -st 1 -rm brownian -xb 0 100 -yb 0 50 --save
```
![Image](examples/100_points_random_brownian.png "Example 4")

You may also generate customised random walk. For example, you can create a simulation with two points with the following
starting points `(0, 10)` and `(50, 50)` with different step lengths `0.5` and `10`.
```
python simulate.py -b 2 -r 10 -s custom -rm brownian -ix 0 50 -iy 10 50 -sts 0.5 10 --save
```
![Image](examples/2_points_custom_brownian.png "Example 5")

Of course everything described above can be done for stochastic process instead of a typical Brownian motion. For
example see the command below which generates a random walk (in a stochastic sense) for 100 blobs.
```
python simulate.py -b 100 -r 100 -s standard -st 1 -rm stochastic --save
```
![Image](examples/100_points_standard_stochastic.png "Example 6")

Till version 1.2 is available it is also possible to create animations. All methods described above are available for both
static and animated plot. Below you can see a command which creates a Brownian motion for 30 points in a form of an animation.
```
python simulate.py -b 25 -r 30 -s standard -rm brownian -pm animated --link --no-grid -i 200 --save
```
![Image](examples/25_points_standard_brownian_animated.gif "Example 7")

Version 1.3 enabled to see how to law of iterated logarithm works which can be fascinating. Please mind that this works
best for stochastic random walk when the start parameter is set to standard and when the distance between two points is equal to 1.
```
python simulate.py -b 100 -r 20 -s standard -rm stochastic --iterated-logarithm -pm animated --save
```
![Image](examples/100_points_standard_stochastic_iterated_logarithm.gif "Example 8")

## Support
If you have any questions about this code feel free to email me @dawidhanrahan@gmail.com.

## License
[MIT](https://gitlab.com/Hendrra/random-walk/-/blob/main/LICENSE)

## Project status

### Current release
<dl>
    <dt>1.0</dt>
    <dd>This version includes the main and the most important mechanism which is capable of simulating a random walk, both Brownian motion and a simple stochastic process. Moreover, there are plenty possibilities to customise the simulation (please see the Usage and Examples). This version includes also a simple method of plotting the random walk. You can think of this version as of a solid source code which can be improved but is capable of creating and plotting simulations.</dd>
    <dt>1.1</dt>
    <dd>
        Improvements:
        <ol>
            <li>enables to run a simulation directly from the terminal using the mechanism of passing the arguments. No need to use, read or even see the source code anymore,</li>
            <li>the created simulations can be saved, more precisely it is possible to save a figure showing the final form of the simulation,</li>
            <li>the missing test for factories have been added.</li>
        </ol>
    </dd>
    <dt>1.2</dt>
    <dd>
        Improvements:
        <ol>
            <li>enables simulations to be viewed and saved (in .gif format) as animations. All previous methods for creating simulations have also been implemented for animations.</li>
        </ol>
    </dd>
    <dt>1.3</dt>
    <dd>
        Improvements:
        <ol>
            <li>adds option to include the iterated logarithm curve.</li>
        </ol>
    </dd>
</dl>

### Future improvements
No further improvements are planned.

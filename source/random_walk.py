class RandomWalk:
    """
    Generates random paths for arbitrary amount of points.

    This class contains the following public methods:
        1) paths: returns the complete paths for each point.
    """

    def __init__(self, x_start, y_start, rounds, d, randomizer, calculator):
        """
        Initialises the instance of the RandomWalk class
        :param x_start: the starting points (x coordinates),
        :param y_start: the starting points (y coordinates),
        :param rounds: the length of the path, the full path will consist of rounds+1 steps (starting point),
        :param d: the distance between two steps,
        :param randomizer: the randomizing function,
        :param calculator: the calculator for new point.
        """
        self._verify(x_start=x_start, y_start=y_start, d=d)

        self.random_paths = [
            SingleRandomWalk(
                x_start=x_start[i],
                y_start=y_start[i],
                rounds=rounds,
                d=d[i],
                randomizer=randomizer,
                calculator=calculator,
            ).coordinates() for i in range(len(x_start))  # here we are sure that len(x_start) == len(y_start)
        ]

    @staticmethod
    def _verify(x_start, y_start, d):
        """
        Verifies the following statements:
            1) the initial coordinates for x and y consist of the same amount of points,
            2) the distance between two steps is greater than 0.
        If any of the statements listed above is not fulfilled a ValueError is raised.
        :param x_start: the list of initial x coordinates,
        :param y_start: the list of initial y coordinates,
        :param d: the distance between two steps.
        """
        if not len(x_start) == len(y_start):
            raise ValueError('The path should contain the same amount of x and y coordinates!')

        if any(step <= 0 for step in d):
            raise ValueError('The distance between two points must be non-negative!')

    def paths(self):
        """
        Returns the random paths.
        :return: list. A list containing tuples. Each tuple contains two lists: one contains the x coordinates
        of a path and the other one contains the y coordinates of the path.
        """
        return self.random_paths


class SingleRandomWalk:
    """
    Generates the path of the random walk, i.e. separately the path
    for x and y coordinates.

    This class contains the following public methods:
        1) coordinates: returns path of the random walk.
    """

    def __init__(self, x_start, y_start, rounds, d, randomizer, calculator):
        """
        Initialises the instance of the SingleRandomWalk class
        :param x_start: the starting point (x coordinate),
        :param y_start: the starting point (y coordinate),
        :param rounds: the length of the path, the full path will consist of rounds+1 steps (starting point),
        :param d: the distance between two steps,
        :param randomizer: the randomizing function,
        :param calculator: the calculator for new point.
        """
        self._x_start = x_start
        self._y_start = y_start
        self._rounds = rounds
        self._d = d

        self._randomizer = randomizer
        self._calculator = calculator

    def coordinates(self):
        """
        Creates the path of the random walk.
        :return: tuple. This tuple contains two lists, one for x coordinates
        and the second for y coordinates.
        """
        x, y = self._x_start, self._y_start
        x_path, y_path = [x], [y]
        for _ in range(self._rounds):
            angle = self._randomizer()
            x, y = self._calculator(x=x, y=y, d=self._d, angle=angle).calculate()
            x_path.append(x)
            y_path.append(y)
        return x_path, y_path

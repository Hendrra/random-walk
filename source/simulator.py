from source.random_walk import RandomWalk
from source.utilities.factories import CalculatorFactory
from source.utilities.factories import ParameterFactory
from source.utilities.factories import PlotterFactory
from source.utilities.factories import RandomizerFactory


class Simulator:
    """
    Gathers required information about a random walk and plots it.

    There are several methods of initialising random walks, please see the description of __init__ function
    for further details.

    Example.

    1) Assume you want to generate a simple random walk. All the points are to start from the same point and all the
    points are to have the same step (i.e. the same distance between the two next steps). In such cases you just need
    to run the following command.

    > RandomWalkPlotter(blobs=5, rounds=5, start='standard', step=1).plot()
    This command will plot a random walk containing 5 different blobs. Trajectory for each blob will contain 6 points
    (5 rounds + initial coordinates). The distance between each two points in the trajectory will be equal to 1 and
    each trajectory will start at (0, 0).

    2) Assume you want to generate a simple random walk but the starting coordinates should be random. Then you
    just need to run the following command.

    > RandomWalkPlotter(blobs=5, rounds=5, start='random', step=1).plot()
    This command will plot a random walk containing 5 different blobs. Trajectory for each blob will contain 6 points
    (5 rounds + initial coordinates). The distance between each two points in the trajectory will be equal to 1 and
    each trajectory will start at a different, randomly chosen point.

    3) You can also customize your random walk. You can set starting coordinates, and you can set different steps for
    each blob. The command below is an example of a custom random walk.

    > RandomWalkPlotter(blobs=3, rounds=5, start='custom', initial_x=[0, 1, 1], initial_y=[0, 1, 1], steps=[1, 1, 3]
    This command will plot a custom random walk.

    This class contains the following public attributes:
        1) paths: the paths (for each point), i.e. the full information about the simulation.

    This class contains the following public methods:
        1) plot: plots the simulation.
    """

    def __init__(self, blobs, rounds, start, step, rand_method, calc_method, x_bounds, y_bounds,
                 initial_x, initial_y, steps, plot_method):
        """
        Initialises the instance of the RandomWalkPlotter class
        :param blobs: number of points participating in a random walk,
        :param rounds: the length of the path for each blob (the length of the walk), this is equivalent to the fact
        that the trajectory of each point will contain rounds+1 spots (the number of rounds + the initial position),
        :param start: the method of computing the starting conditions for points involved in the random walk, there
        are several options provided:
            1) standard: the most expected method for creating random walks, here we have specified amount of points
            (argument blobs), each point has the same step length (argument d), moreover all the points start from
            the same starting coordinates, i.e. (0, 0). Please note that this method requires the following arguments:
            blobs and d,
            2) random: very similar to standard with one major difference -- starting points are randomly chosen for
            each of the point. Please note that this method requires the following arguments: blobs and d. You can also
            specify x_bounds and y_bounds (by default a square),
            3) custom: you can customise everything, i.e. you can manually set the starting coordinates for both axis,
            you can also manually set the length of the step for each point, if you select this method you need to
            provide the following arguments: blobs, initial_x, initial_y and steps. Note! If any of those lists contains
            fewer elements than the value of blobs than the list will be completed with the last element from it which
            was provided by the user. For example let's assume that the user typed the following arguments: blobs=5
            and initial_x=[0, 1, 2]. In such cases random walk will be computed with initial_x=[0, 1, 2, 2, 2],
        :param step: the distance between two steps, this parameter is used for start=standard and start=random,
        :param rand_method: the type of the random walk, for now there are two possibilities:
            1) brownian: standard random walk, i.e. brownian motion(s) will be plotted,
            2) stochastic: the class will plot standard stochastic process(es),
        :param calc_method: the method of computing new, next points in the random walk; for now there
        is only one possibility:
            1) standard: for further details please check the description inside StandardCoordinatesCalculator class,
        :param x_bounds: the boundaries for x coordinates, i.e. when start=random the x coordinates are drawn
        between the specified numbers,
        :param y_bounds: the boundaries for y coordinates, i.e. when start=random the y coordinates are drawn
        between the specified numbers,
        :param initial_x: the list of initial x coordinates, you need to specify the list if and only if start=custom,
        :param initial_y: the list of initial y coordinates, you need to specify the list if and only if start=custom,
        :param steps: the list of the lengths of the steps for each point, you need to specify this argument
        if and only if start=custom,
        :param plot_method: variable which indicates the type of the plot, for now there's one possibility:
            1) static: just the final result of the simulation is going to be plotted.
        """
        self.blobs = blobs

        _randomizer = RandomizerFactory(method=rand_method).produce()
        _calculator = CalculatorFactory(method=calc_method).produce()
        _parameters = ParameterFactory(
            method=start,
            blobs=blobs,
            step=step,
            x_bounds=x_bounds,
            y_bounds=y_bounds,
            initial_x=initial_x,
            initial_y=initial_y,
            steps=steps
        ).produce()
        self._plotter = PlotterFactory(method=plot_method).produce()

        self.paths = RandomWalk(
            x_start=_parameters['x_start'],
            y_start=_parameters['y_start'],
            rounds=rounds,
            d=_parameters['d'],
            randomizer=_randomizer,
            calculator=_calculator,
        ).paths()

    def plot(self, link, annotate, grid, save, interval, iterated_logarithm, alpha=0.5):
        """
        Plots the simulation using the proper plotting class
        :param link: indicates whether the points are to be connected,
        :param annotate: indicates whether the points are to be labelled,
        :param grid: indicates whether the graph should have a grid,
        :param save: indicates whether the figure should be saved, if True the figure will be
        saved in the figures/ directory,
        :param interval: delay between frames in milliseconds,
        :param iterated_logarithm: indicates whether to add the iterated logarithm curve,
        :param alpha: transparency index; by default equals to 0.5.
        """
        self._plotter(
            paths=self.paths, link=link, annotate=annotate, grid=grid,
            save=save, alpha=alpha, interval=interval, iterated_logarithm=iterated_logarithm,
        )

import argparse


class Parser:
    """
    Parses the arguments provided by the user in the terminal.

    The Parser contains the following public methods:
        1) parse -- returns parsed arguments in a form of a Namespace.
    """

    @classmethod
    def parse(cls, arguments=None):
        """
        Parses the arguments, i.e. creates the parser and enables the class to parse arguments
        from the terminal. Returns them as a Namespace.
        :return: Namespace.
        """
        parser = argparse.ArgumentParser(description='Argument parser for the Random Walk simulator.')

        help_message_for_blobs = 'Number of points participating in a random walk. Required.'
        parser.add_argument(
            '-b', '--blobs',
            help=help_message_for_blobs,
            type=int,
            required=True,
        )

        help_message_for_rounds = (
            'The length of the path for each blob (the length of the walk), this is equivalent to the fact that the '
            'trajectory of each point will contain of rounds+1 spots (the number of rounds + initial position).'
            'Required.'
        )
        parser.add_argument(
            '-r', '--rounds',
            help=help_message_for_rounds,
            type=int,
            required=True,
        )

        help_message_for_start = (
            'The method of computing the starting conditions for points involved in the random walk. For details '
            'please see the description of Simulator in the documentation. Required.'
        )
        parser.add_argument(
            '-s', '--start',
            help=help_message_for_start,
            type=str,
            required=True,
            choices=['standard', 'random', 'custom']
        )

        help_message_for_step = (
            'The distance between two steps, this parameter is used for start=standard and start=random, by '
            'default set to 1.'
        )
        parser.add_argument(
            '-st', '--step',
            help=help_message_for_step,
            type=float,
            default=1,
        )

        help_message_for_rand_method = (
            'The type of the random walk. For details please see the description of Simulator in the documentation, '
            'by default set to brownian.'
        )
        parser.add_argument(
            '-rm', '--rand-method',
            help=help_message_for_rand_method,
            type=str,
            choices=['brownian', 'stochastic'],
            default='brownian',
        )

        help_message_for_calc_method = (
            'The method of computing new, next points in the random walk. For details please see the description of '
            'Simulator in the documentation, by default set to standard.'
        )
        parser.add_argument(
            '-cm', '--calc-method',
            help=help_message_for_calc_method,
            type=str,
            choices=['standard'],
            default='standard',
        )

        help_message_for_x_bounds = (
            'The boundaries for x coordinates, i.e.when start=random the x coordinates are drawn between the '
            'specified numbers, by default equals to (-20, 20).'
        )
        parser.add_argument(
            '-xb', '--x-bounds',
            help=help_message_for_x_bounds,
            nargs='+',
            type=float,
            default=(-20, 20),
        )

        help_message_for_y_bounds = (
            'The boundaries for y coordinates, i.e.when start=random the y coordinates are drawn between the '
            'specified numbers, by default equals to (-20, 20).'
        )
        parser.add_argument(
            '-yb', '--y-bounds',
            help=help_message_for_y_bounds,
            nargs='+',
            type=float,
            default=(-20, 20),
        )

        help_message_for_initial_x = (
            'The list of initial x coordinates, you need to specify the list if and only if start=custom, by '
            'default set to None.'
        )
        parser.add_argument(
            '-ix', '--initial-x',
            help=help_message_for_initial_x,
            nargs='+',
            type=float,
            default=None,
        )

        help_message_for_initial_y = (
            'The list of initial y coordinates, you need to specify the list if and only if start=custom, by '
            'default set to None.'
        )
        parser.add_argument(
            '-iy', '--initial-y',
            help=help_message_for_initial_y,
            nargs='+',
            type=float,
            default=None,
        )

        help_message_for_steps = (
            'The list of the lengths of the steps for each point, you need to specify this argument if and only '
            'if start=custom, by default set to None.'
        )
        parser.add_argument(
            '-sts', '--steps',
            help=help_message_for_steps,
            nargs='+',
            type=float,
            default=None,
        )

        help_message_for_plot_method = (
            'Indicates the type of the plot. For details please see the description of Simulator in the documentation, '
            'by default set to static.'
        )
        parser.add_argument(
            '-pm', '--plot-method',
            help=help_message_for_plot_method,
            type=str,
            default='static',
        )

        help_message_for_link = (
            'Indicates whether the points are to be connected. In other words if set to True the points participating '
            'in the random walk will be linked. They will form a path.'
        )
        parser.add_argument(
            '--link',
            help=help_message_for_link,
            action=argparse.BooleanOptionalAction,
            default=True,
        )

        help_message_for_annotate = (
            'Indicates whether the points are to be labelled. In other words if set to True the points participating '
            'in the random walk will be denoted by consecutive natural numbers.'
        )
        parser.add_argument(
            '--annotate',
            help=help_message_for_annotate,
            action=argparse.BooleanOptionalAction,
            default=False,
        )

        help_message_for_grid = (
            'Indicates whether the graph should have a grid.'
        )
        parser.add_argument(
            '--grid',
            help=help_message_for_grid,
            action=argparse.BooleanOptionalAction,
            default=True,
        )

        help_message_for_save = (
            'Indicates whether the figure should be saved, if True the figure will be saved in the figures/ directory.'
        )
        parser.add_argument(
            '--save',
            help=help_message_for_save,
            action=argparse.BooleanOptionalAction,
            default=False,
        )

        help_message_for_interval = 'Delay between frames in milliseconds. By default equals to 100.'
        parser.add_argument(
            '-i', '--interval',
            help=help_message_for_interval,
            type=float,
            default=100,
        )

        help_message_for_iterated_logarithm = (
            'If True adds the iterated logarithm curve. Please note that this makes sense if and only if the round '
            'method is equal to stochastic. Please use this parameter when step is equal to 1.'
        )
        parser.add_argument(
            '--iterated-logarithm',
            help=help_message_for_iterated_logarithm,
            action=argparse.BooleanOptionalAction,
            default=False,
        )

        return parser.parse_args(arguments)

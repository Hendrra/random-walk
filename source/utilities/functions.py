import math


def iterated_logarithm(n):
    """
    Computes the iterated logarithm
    :param n: arbitrary number,
    :return: the value of the iterated logarithm.
    """
    return math.sqrt(2 * n * math.log(math.log(n)))

import random


def dummy_randomizer():
    """
    Randomizer which is not random. Used for testing purposes.
    :return: int.
    """
    return 0


def random_float(left_bound, right_bound):
    """
    Generates a random float number between a given range. The interval
    is bounded from the left and from the right; the left bound is reachable
    however the right bound is not.
    Please note that in case the left bound is greater than the right bound
    the function raises an error with a specific message
    :param left_bound: left end of the interval of the drawn numbers,
    :param right_bound: right end of the interval of the drawn numbers,
    :return: float.
    """
    if left_bound > right_bound:
        raise ValueError('Left bound cannot be greater than right bound!')

    return left_bound + random.random() * (right_bound - left_bound)


def randomizer_for_brownian_motion():
    """
    Specialised randomizer used for generating a standard brownian motion.
    :return: float.
    """
    return random_float(left_bound=0, right_bound=360)


def randomizer_for_stochastic_process():
    """
    Specialised randomizer used for generating a standard stochastic process.
    :return: float.
    """
    return random_float(left_bound=-90, right_bound=90)

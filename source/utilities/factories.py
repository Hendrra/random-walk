import abc

from source.utilities.calculators import StandardCoordinatesCalculator
from source.utilities.plotters import AnimatedPlotter
from source.utilities.plotters import StaticPlotter
from source.utilities.randomizers import random_float
from source.utilities.randomizers import randomizer_for_brownian_motion
from source.utilities.randomizers import randomizer_for_stochastic_process


class Factory(metaclass=abc.ABCMeta):
    """
    Base class for all factories used in the Simulator class.

    All child instances will contain the following public methods:
        1) produce: produces the method, class, etc. based on
        the method.
    """

    def __init__(self, method):
        """
        Initialises the Factory instance
        :param method: parameter which indicates what method is used to compute something.
        """
        self._method = method

    @abc.abstractmethod
    def produce(self):
        pass


class CalculatorFactory(Factory):
    """
    Factory for calculators for coordinates.

    Supported methods:
        1) standard: for further details please see StandardCoordinatesCalculator.
    """

    def produce(self):
        """
        Returns the class which is responsible for computing the new coordinates.
        Please note that if unsupported method is provided a ValueError is raised.
        :return: calculator class.
        """
        if self._method == 'standard':
            return StandardCoordinatesCalculator
        raise ValueError('Supported methods: standard!')


class RandomizerFactory(Factory):
    """
    Factory for randomizers, i.e. functions which add randomness to the random walk.

    Supported methods:
        1) brownian: for further details please see randomizer_for_brownian_motion,
        2) stochastic: for further details please see randomizer_for_stochastic_process.
    """

    def produce(self):
        """
        Returns the function which is responsible for adding the randomness.
        :return: function.
        """
        if self._method == 'brownian':
            return randomizer_for_brownian_motion
        if self._method == 'stochastic':
            return randomizer_for_stochastic_process
        raise ValueError('Supported methods: brownian and stochastic!')


class ParameterFactory(Factory):
    """
    Factory for parameters which will generate a random walk.

    Supported methods:
        1) standard: for further details please see the description in Simulator class,
        2) random: for further details please see the description in Simulator class,
        3) custom: for further details please see the description in Simulator class.
    """

    def __init__(self, method, blobs, step, x_bounds, y_bounds, initial_x, initial_y, steps):
        """
        Initialises the ParameterFactory instance
        :param method: parameter which indicates what method is used to compute something,
        :param blobs: number of points participating in a random walk,
        :param step: the distance between two steps,
        :param x_bounds: the boundaries for x coordinates,
        :param y_bounds: the boundaries for y coordinates,
        :param initial_x: the list of initial x coordinates,
        :param initial_y: the list of initial y coordinates,
        :param steps: the list of the lengths of the steps for each point.
        """
        super().__init__(method=method)
        self._dimensions = 2
        self._blobs = blobs
        self._step = step
        self._bounds = [x_bounds, y_bounds]
        self._custom = [initial_x, initial_y, steps]

    def produce(self):
        """
        Returns the dictionary containing the following keys:
            1) x_start: the x starting coordinate,
            2) y_start: the y starting coordinate,
            3) d: the list of steps for each point.
        :return: dictionary.
        """
        producer = getattr(self, '_' + self._method + '_producer')
        x, y, steps = producer()
        return {'x_start': x, 'y_start': y, 'd': steps}

    def _standard_producer(self):
        parameters = []
        for value in [0, 0, self._step]:
            parameters.append(
                [value for __ in range(self._blobs)]
            )
        return parameters

    def _random_producer(self):
        parameters = []
        for bound in self._bounds:
            left_bound, right_bound = bound
            parameters.append(
                [random_float(left_bound=left_bound, right_bound=right_bound) for _ in range(self._blobs)]
            )
        parameters.append([self._step for _ in range(self._blobs)])
        return parameters

    def _custom_producer(self):
        parameters = []
        for values in self._custom:
            n = len(values)
            if n < self._blobs:
                values = values + [values[-1]] * (self._blobs - n)
            if n > self._blobs:
                values = values[:self._blobs]
            parameters.append(values)
        return parameters


class PlotterFactory(Factory):
    """
    Factory for the plotting class.

    Supported methods:
        1) static: for further details please see the description in StaticPlotter class,
        2) animated: for further details please see the description in AnimatedPlotter class.
    """

    def produce(self):
        """
        Returns the class which is responsible for plotting the simulation.
        :return: plotting class.
        """
        if self._method == 'static':
            return StaticPlotter
        if self._method == 'animated':
            return AnimatedPlotter
        raise ValueError('Supported methods: static and animated!')

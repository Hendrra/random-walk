import math


class StandardCoordinatesCalculator:
    """
    Calculates the new coordinates based on the previous one, the length
    of the step and the chosen angle.

    This class contains several public attributes:
        1) x: the starting x coordinate,
        2) y: the starting y coordinate,
        3) d: the distance between two next steps,
        4) psi: the angle (between the previous and the next step).

    This class contains one public method:
        1) calculate: calculates the new coordinates.
    """

    def __init__(self, x, y, d, angle, degrees=True):
        """
        Initialises the CoordinatesCalculator
        :param x: the x coordinate from the previous step,
        :param y: the y coordinate from the previous step,
        :param d: the length of the step,
        :param angle: the randomly chosen angle,
        :param degrees: indicator whether the angle is given in degrees or in radians.
        """
        self.x = x
        self.y = y
        self.d = d
        self.psi = angle if not degrees else math.radians(angle)

    def calculate(self):
        """
        Calculates the new coordinates. The calculation method is really simple,
        we assume that the previous point is the new origin of the coordinate system,
        based on that we compute the new coordinates. Afterwards we move the
        coordinate system so that it is consistent with the original one.
        :return: tuple; the (x, y) coordinates.
        """
        x = self.d * math.cos(self.psi) + self.x
        y = self.d * math.sin(self.psi) + self.y
        return x, y

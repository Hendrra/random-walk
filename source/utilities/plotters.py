import abc
import os
from datetime import datetime

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.animation import PillowWriter

from source.utilities import functions


class PlotterBase(metaclass=abc.ABCMeta):
    """
    Base class for Plotters. Plotter classes are responsible for plotting the simulations.
    """

    _extension = None  # this attribute is used to save the plot in a proper format

    def __init__(self, paths, link, annotate, grid, save, interval, iterated_logarithm, alpha):
        """
        Initialises the Plotter instance
        :param paths: the paths for each point which participant in the random walk,
        :param link: indicates whether the points are to be connected,
        :param annotate: indicates whether the points are to be labelled,
        :param grid: indicates whether the graph should have a grid,
        :param save: indicates whether the figure should be saved, if True the figure will be
        saved in the figures/ directory,
        :param interval: delay between frames in milliseconds,
        :param iterated_logarithm: dataset for plotting the iterated logarithm,
        :param alpha: transparency index.
        """
        self._paths = paths
        self._link = link
        self._annotate = annotate
        self._grid = grid
        self._alpha = alpha
        self._interval = interval
        self._iterated_logarithm = iterated_logarithm

        self._blobs = len(paths)
        self._length = len(paths[0][0])  # here we are sure that the paths are computed correctly

        self._fig, self._ax = plt.subplots()
        self._animation = None

        self._plot()

        if save:
            self._check_path()
            filename = self._create_filename()
            self._save(filename=filename)

    @abc.abstractmethod
    def _plot(self):
        pass

    def _fill_plot(self, x, y, boundary, title=None):
        if title:
            self._ax.set_title(title)

        if self._link:
            self._ax.plot(
                x, y, alpha=self._alpha, linestyle='--', marker='o'
            )
        else:
            self._ax.scatter(
                x, y, alpha=self._alpha
            )

        if self._annotate:
            for i in range(len(x)):
                self._ax.annotate(i+1, (x[i], y[i]))

        if self._iterated_logarithm:
            domain = [_ for _ in range(4, boundary+1)]
            plus_values = [
                functions.iterated_logarithm(n) for n in domain
            ]
            minus_values = [
                -functions.iterated_logarithm(n) for n in domain
            ]
            self._ax.plot(
                domain, plus_values, c='crimson', ls='--', linewidth=3
            )
            self._ax.plot(
                domain, minus_values, c='crimson', ls='--', linewidth=3
            )

    @abc.abstractmethod
    def _save(self, filename):
        pass

    @staticmethod
    def _check_path():
        if not os.path.exists('figures/'):
            os.mkdir('figures/')

    def _create_filename(self):
        filename = 'figures/{name}.{extension}'
        name = datetime.now().strftime("%d-%m-%Y %H:%M:%S")
        return filename.format(name=name, extension=self._extension)


class StaticPlotter(PlotterBase):
    """
    Class used for plotting a random walk in a static manner, i.e. this class
    is capable of plotting the final result of the simulation.
    """

    _extension = 'png'

    def _plot(self):
        if self._grid:
            self._ax.grid()

        boundary = int(max(list(map(lambda x: max(x[0]), self._paths))))
        for path in self._paths:
            self._fill_plot(x=path[0], y=path[1], boundary=boundary)

        plt.show()

    def _save(self, filename):
        self._fig.savefig(filename)


class AnimatedPlotter(PlotterBase):
    """
    Class used for plotting a random walk as an animation, i.e. this class
    is capable of plotting the random walk step by step.
    """

    _extension = 'gif'

    def _plot(self):
        self._animation = FuncAnimation(
            self._fig, self._anim,  interval=self._interval, frames=self._length, repeat=False,
        )
        plt.show()

    def _anim(self, frame):
        self._ax.clear()

        if self._grid:
            self._ax.grid()

        for path in self._paths:
            boundary = int(max(list(map(lambda x: max(x[0][:frame+1]), self._paths))))
            self._fill_plot(x=path[0][:frame+1], y=path[1][:frame+1], boundary=boundary, title=f'Step: {frame}')

    def _save(self, filename):
        self._animation.save(filename, dpi=300, writer=PillowWriter(fps=25))

from source.simulator import Simulator
from source.parser import Parser

if __name__ == '__main__':
    arguments = Parser().parse()
    Simulator(
        blobs=arguments.blobs,
        rounds=arguments.rounds,
        start=arguments.start,
        step=arguments.step,
        rand_method=arguments.rand_method,
        calc_method=arguments.calc_method,
        x_bounds=arguments.x_bounds,
        y_bounds=arguments.y_bounds,
        initial_x=arguments.initial_x,
        initial_y=arguments.initial_y,
        steps=arguments.steps,
        plot_method=arguments.plot_method,
    ).plot(
        link=arguments.link,
        annotate=arguments.annotate,
        grid=arguments.grid,
        save=arguments.save,
        interval=arguments.interval,
        iterated_logarithm=arguments.iterated_logarithm,
    )
